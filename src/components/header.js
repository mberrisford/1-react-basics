import React, { Component } from 'react';

class Header extends Component {

    constructor(props) {
        super(props);

        this.state = {
            title: 'The keywords are:',
            keywords: ''
        }
    }

    inputChange(event) {
        // console.log(event.target.value)

        this.setState({ keywords: event.target.value })
    }


    enterPressed(event) {
        if (event.key === 'Enter') {
            console.log('Enter pressed')
        }
    }

    render() {

        return (
            // <header style={styles.header}>
            <header>
                <div className='logo'
                    onClick={() => console.log('logo clicked')}
                >Logo</div>
                <input onChange={this.inputChange.bind(this)}
                    onKeyPress={this.enterPressed}
                />
                <div>{this.state.title}</div>
                <div>{this.state.keywords}</div>
            </header>
        )
    }

}

export default Header;